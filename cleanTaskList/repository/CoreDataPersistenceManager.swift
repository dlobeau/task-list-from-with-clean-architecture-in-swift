//
//  CoreDataPersistenceManager.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 14/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import CoreData

class PersistanceManager
{
    // MARK: - Core Data stack

    var storeDescription:String
    var shouldAddStoreAsynchronously:Bool
    
    init(WithStoreDescription StoreDescription:String, WithShouldAddStoreAsynchronously ShouldAddStoreAsynchronously:Bool )
    {
        self.storeDescription = StoreDescription
        self.shouldAddStoreAsynchronously = ShouldAddStoreAsynchronously
    }
    
    lazy var persistentContainer: NSPersistentContainer =
    {
        let container = NSPersistentContainer(name: "cleanFrameworkRepository", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = self.storeDescription
        description.shouldAddStoreAsynchronously = self.shouldAddStoreAsynchronously
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == self.storeDescription )
                                        
            // Check if creating container wrong
            if let error = error {
                fatalError("Create an in-mem coordinator failed \(error)")
            }
        }
        
        return container
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()
}
