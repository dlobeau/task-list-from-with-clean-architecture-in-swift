//
//  Repository.swift
//  cleanRepositoryManagement
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import CoreData

protocol Repository
{
    func prepareRepository()
    
    func create(withItemDictionary ItemDictionary:[String:String])
    func request(withItemGroupId GroupId:String,withItemId Id:String )->(SimpleComposite?)
    func update(withItem Item:SimpleComposite)
    func delete(withItem Item:SimpleComposite)
}

class RepositoryCoreData:Repository
{
    
    var persistentStore:NSPersistentContainer
    
    init()
    {
          let persistentStoreMager = PersistanceManager.init(WithStoreDescription: NSSQLiteStoreType, WithShouldAddStoreAsynchronously: false)
          
          persistentStore = persistentStoreMager.persistentContainer
    }
      
    func create(withItemDictionary ItemDictionary:[String:String])
    {
        
        let Context = self.persistentStore.viewContext
           
        
        let ManagedObject = NSEntityDescription.insertNewObject(forEntityName: "SimpleComposite", into: Context)
        
         
        if let compositeObject = ManagedObject as? SimpleComposite
        {
            compositeObject.label = ItemDictionary["label"]!
            compositeObject.identifier = ItemDictionary["identifier"]!
            compositeObject.group_id = ItemDictionary["group_id"]!
            compositeObject.type_id = ItemDictionary["type_id"]!
        }
        
        self.save()
    }
    
    func request(withItemGroupId GroupId: String, withItemId Id: String) -> (SimpleComposite?)
    {
        var ReturnValue:SimpleComposite? = nil
        do
        {
            let Tasks = try self.persistentStore.viewContext.fetch(SimpleComposite.fetchRequest()) as? [SimpleComposite]
        
            ReturnValue = Tasks!.first(where:{($0.group_id == GroupId) && ($0.type_id == Id)})
        }
        catch
        {
            
        }
        return ReturnValue
    }
    
    func update(withItem Item:SimpleComposite)
    {
        
    }
    
    func delete(withItem Item:SimpleComposite)
    {
        
    }
     
    func prepareRepository()
    {
        
    }
   
    // MARK: - Core Data Saving support

    func save()
    {
        let context = self.persistentStore.viewContext
        if context.hasChanges
        {
            do {
                try context.save()
            } catch
            {
              
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}







