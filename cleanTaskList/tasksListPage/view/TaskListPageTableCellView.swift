//
//  TaskListPageTableCell.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 23/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class TaskListPageTableCell: TLUITableViewCellFacade
{
    
    @IBOutlet weak var checkBox: TaskListTableCellCheckBoxView!
    @IBOutlet weak var deadlineLabel: KAUILabel!
    @IBOutlet weak var nameLabel: KAUILabel!
    
    func clickWithSender( Sender:KAView)->()
    {
        self.checkBox!.tickCheckBox()
        self.interface()?.addCommand(onChildPresenterModification: self.checkBox!.interface(), withSender: self.interface())
        self.interface()!.executeCommandHierarchy()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }

    override func setContent()
    {
        deadlineLabel.setContent()
        nameLabel.setContent()
        checkBox.setContent()
    }
}
