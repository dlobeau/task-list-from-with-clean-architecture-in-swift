//
//  TaskListPageTable.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 23/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class TaskListPageTableView:KAUITableView
{
    override func setContent() {
        super.reloadData()
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let Table = tableView as! TaskListPageTableView
        let Cell = tableView.cellForRow(at: indexPath) as! TaskListPageTableCell
        
        Cell.clickWithSender(Sender: Table)
    }
}
