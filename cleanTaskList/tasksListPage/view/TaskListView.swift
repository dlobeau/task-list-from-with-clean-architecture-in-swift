//
//  TaskListView.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 23/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class TaskListViewController:TLUIFacadeViewController
{
    @IBOutlet weak var table: TaskListPageTableView!
    var rightBarButton:TLUIBarButtonFacade?
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        self.interface().dataBinding()
        self.setContent()
    }
    
    override func setContent()
    {
        super.setContent();
        self.table.setContent()
    }
    
    override func addChildView(_ ChildView: KAView!) -> Bool
    {
        let returnValue = true;
            
        if( "rightButton" == ChildView.interface()?.id()  )
        {
            self.rightBarButton = ChildView as? TLUIBarButtonFacade
            self.rightBarButton!.action =  #selector(rightButtonAction)
            self.rightBarButton!.target = self
            let BarItem = self.navigationItem;
            BarItem.rightBarButtonItem = self.rightBarButton;
        }
        ChildView.setDelegatePushNext?(self.delegatePushNextView())
            
        return returnValue;
        
    }
    
    @objc func rightButtonAction(sender: TLUIBarButtonFacade)
    {
        self.rightBarButton?.action(fromOwnerWindow: self)
    }
    
}
