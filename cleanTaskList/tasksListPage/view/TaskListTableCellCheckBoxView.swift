//
//  TaskListTableCellCheckBoxView.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 30/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class TaskListTableCellCheckBoxView: TLUIViewFacade
{
    @IBOutlet weak var checkMarkerView: UIView!
    
    override func setContent()
    {
       let CheckBox = self.interface() as! KACheckBox
        self.checkMarkerView.isHidden = CheckBox.value();
    }

    func tickCheckBox()->()
    {
        self.checkMarkerView.isHidden = !self.checkMarkerView.isHidden;
        
        let CheckBox = self.interface()
        CheckBox?.addCommandOnPresenterModification(withBoolean: !self.checkMarkerView.isHidden, withSender: CheckBox)
        CheckBox?.executeCommandHierarchy()
    }

    func isCheckBocxTicked()->(Bool)
    {
        return !self.checkMarkerView.isHidden;
    }

}
