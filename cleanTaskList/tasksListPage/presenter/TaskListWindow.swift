//
//  TaskListWindow.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TaskListWindow:KAPresenter,KASeriazableObject
{
    var table:TasksListTable? { get }
    var addButton:TLBarButton? { get }
    
    var taskCreationPage:TaskDescriptionPage?{get};
}

class TaskListWindowPresenter: KAGenericPresenter,TaskListWindow
{
    var addButton: TLBarButton?
    {
        return self.getChildwithIdentifier( "addBarButton") as? TLBarButton
    }
    
    var taskCreationPage: TaskDescriptionPage?
    {
        var ReturnValue:TaskDescriptionPage?
        
        let Event = self.addButton?.event?()
          
        ReturnValue = Event?.getDestinationWithOwner(self.addButton!) as? TaskDescriptionPage
        
        return ReturnValue
    }
    
    var table: TasksListTable?
    {
        return self.getChildwithIdentifier("taskListTable") as? TasksListTable
    }
    
    static override func getTypeTag() -> String! {
           return "taskListWindow"
       }
    
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(TaskListWindowPresenterConformToDomain.init())
        self.setViewFactoryDelegate(KACreateWindowFactory.init())
       
    }
    
}

class TaskListWindowPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let PAge = Sender as! TaskListWindow
        let DataBase = Domain as! TaskDataBase
        
         
        let Delegate = PAge.table!.testdelegate?()
        
        Delegate?.expectSender(PAge.table!, conformTo: DataBase)
    }
}

