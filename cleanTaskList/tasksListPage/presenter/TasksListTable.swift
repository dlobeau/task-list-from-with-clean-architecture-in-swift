//
//  TasksListTable.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TasksListTable:KATable,KASeriazableObject
{
    var allTaskSection:TasksListSection? {get}
}

class TasksListTablePresenter:KATablePresenter,TasksListTable
{
    var allTaskSection: TasksListSection?
    {
        get{
            return super.section(at: 0) as? TasksListSection;
        }
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(TaskListTablePresenterConformToDomain.init())
        self.setViewFactoryDelegate(KACreateUIWidgetTable.init())
    }
    
    static override func getTypeTag() -> String! {
        return "taskListTable"
    }
    
}


class TaskListTablePresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Table = Sender as! TasksListTable
        let DataBase = Domain as! TaskDataBase
        
        let SectionsDomainReference = DataBase.request(withID: "allTasks")
        
        let Section = Table.allTaskSection!
        let Delegate = Section.testdelegate?()
        
        Delegate?.expectSender(Section, conformTo: SectionsDomainReference!)
    }
}
