//
//  TasksListTableCell.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TasksListTableCell:KACell,KASeriazableObject
{
    var deadlineValueLabel:KALabel?{get}
    
    var nameValueLabel:KALabel?{get}
    
    var taskStatusCheckBox:TaskListTableCellCheckBox?{get}
    
    var editTaskWindow:TaskDescriptionPage?{get}
}


class TasksListTableCellPresenter:KACellPresenter,TasksListTableCell
{
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
       {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
           self.setTestdelegate(TasksListTableCellPresenterConformToDomain.init())
           
       }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
       }
    
    static override func getTypeTag() -> String!
    {
        return "taskListTableCell"
    }
    var deadlineValueLabel: KALabel?
    {
        get
        {
            return self.getChildwithIdentifier("value1") as? KALabel
        }
    }
    var nameValueLabel: KALabel?
     {
         get
         {
             return self.getChildwithIdentifier("value0") as? KALabel
         }
     }
    
    var taskStatusCheckBox: TaskListTableCellCheckBox?
    {
        get
        {
            return self.getChildwithIdentifier("checkBox") as? TaskListTableCellCheckBox
        }
    }
    
    var editTaskWindow:TaskDescriptionPage?
    {
        get
        {
            var ReturnValue:TaskDescriptionPage?
            let ListSwipAction = self.swipLeftEvents()
            if(ListSwipAction.count > 0)
            {
                let EditSwapAction = ListSwipAction[0]
                let Event = EditSwapAction.event?();
                ReturnValue = Event?.getDestinationWithOwner(self) as? TaskDescriptionPage
            }
            
            return ReturnValue;
        }
    }

    
    
}

class TasksListTableCellPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Cell = Sender as! TasksListTableCell
        let TaskReference = Domain as! Task
        
        let CheckDeadlineLabelConformity = Cell.deadlineValueLabel!.testdelegate?()
        CheckDeadlineLabelConformity?.expectSender?(Cell.deadlineValueLabel!, conformToText: (TaskReference.deadline?.completeString())!)
        
        let CheckNameLabelConformity = Cell.nameValueLabel!.testdelegate?()
        CheckNameLabelConformity?.expectSender?(Cell.nameValueLabel!, conformToText: TaskReference.name!)
        
        let CheckBoxConformity = Cell.taskStatusCheckBox!.testdelegate?()
        CheckBoxConformity?.expectSender(Cell.taskStatusCheckBox!,conformTo:TaskReference.status!)
        
    }
}



