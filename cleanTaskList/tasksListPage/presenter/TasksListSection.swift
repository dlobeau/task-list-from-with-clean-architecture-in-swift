//
//  TasksListSection.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TasksListSection:KASection,KASeriazableObject
{
    
}



class TaskListSectionPresenter: KASectionPresenter,TasksListSection
{
    static override func getTypeTag() -> String!
    {
           return "taskListTableAllTasksSection"
       }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(TaskListSectionPresenterConformToDomain.init())
        
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
       }
}

class TaskListSectionPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Section = Sender as! TasksListSection
        let List = Domain as! TaskList
             
        let Tasks =  List.tasks()
        let Cells = Section.cells()
        
        assert(Tasks.count == Cells.count, "Cells number and task number not equal, \(Tasks.count), and \( Cells.count )")
        var i = 0;
        for Cell in Cells
        {
            let CheckConformity = Cell.testdelegate?()
            CheckConformity!.expectSender(Cell, conformTo: Tasks[i])
            i+=1
        }
    }
}


