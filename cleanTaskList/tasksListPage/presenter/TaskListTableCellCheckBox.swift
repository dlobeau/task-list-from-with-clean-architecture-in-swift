//
//  TaskListTableCellCheckBox.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 30/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TaskListTableCellCheckBox:KACheckBox,KASeriazableObject {
    
}

class TaskListTableCellCheckBoxPresenter:KACheckBoxPresenter,TaskListTableCellCheckBox
{
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
       {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
           
           
       }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
       }
    
    static override func getTypeTag() -> String!
    {
        return "taskListTableCellCheckBox"
    }
}
