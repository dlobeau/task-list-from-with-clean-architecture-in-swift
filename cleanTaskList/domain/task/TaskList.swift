//
//  TaskList.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol TaskList:KADomain
{
    func tasks()->([Task]);
    func addTask(aTask:Task);
}

class TaskListGeneric: KAGenericDomain,TaskList
{
    
   lazy private var taskLists = [Task]()
    
    func tasks()->([Task])
    {
       return self.taskLists;
    }

    func addTask(aTask:Task)
    {
        self.taskLists.append(aTask)
    }

    override func childs() -> [KADomain]!
    {
       return self.taskLists;
    }

    static func createList()->(Any?)
    {
        return TaskListGeneric.init(label: "taskList", withLabelIdentifier: "taskList" , withObjectFamilyName: "taskList", withID: "taskList");
    }

    override func request(withID RequestID: String!) -> KADomain!
    {
        var ReturnValue:KADomain?
        
        switch RequestID {
        case "allTasks":
            ReturnValue = self;
        default:
            ReturnValue = super.request(withID: RequestID)
        }
        return ReturnValue;
    }

}
