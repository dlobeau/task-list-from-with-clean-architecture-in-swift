//
//  Task.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 25/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol Task:KADomain
{
    var name:String?
    {
        get
        set
    }
    
    func taskId(TaskId ID:String) -> (String)
    
    var  deadline:KADate?
    {
        get
        set
    }
    
    var status:KABoolean?
    {
        get
        set
    }
    
    var owner:TaskDataBase?
    {
        get
        set
    }
    
}

class TaskGeneric: KAGenericDomain,Task
{
    static let NAME_ID = "name";
    static let DEADLINE_ID = "deadline";
    static let STATUS_ID = "status";
    
    func taskId(TaskId ID: String) -> (String) {
        return super.id()
    }
    
    private var statusObject:KABoolean?
    
    var name: String?
    {
        get
        {
            return super.label()
        }
        set(newValue)
        {
            super.setLabel(newValue!)
        }
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
       
    }
    
    var deadline: KADate?
    
    var status: KABoolean?
    {
        get
        {
            if(statusObject == nil)
            {
                statusObject = super.getChildwithTypeId("value1") as? KABoolean
                if(statusObject == nil)
                {
                    statusObject = self.factoryDelegate()?.createObject(fromFamilyType: "bool") as? KABoolean
                    statusObject!.setValue(false)
                    self.status = statusObject
                }
            }
            return statusObject
        }
        set(newValue)
        {
            statusObject = newValue?.cloneObject() as? KABoolean
            statusObject!.setID("status")
            self.addChild(statusObject) 
        }
    }
    
    var owner: TaskDataBase?
    
    init( WithName Name:String, WithDeadLine Deadline:KADate)
    {
        super.init(label: Name, withLabelIdentifier: Name, withObjectFamilyName: "task", withID: "task")
        self.name = Name
        self.deadline = Deadline
    }
    
    override func compareAttribute(_ AttributeToBeCompared: KAComparable!) -> ComparisonResult
    {
        let Parameter = AttributeToBeCompared as! Task
        var Result = ComparisonResult.orderedAscending;
        
        if( (self.name == Parameter.name)
            && (self.deadline!.compareAttribute(Parameter.deadline!) == ComparisonResult.orderedSame) )
        {
            Result = ComparisonResult.orderedSame;
        }
        return Result;
    }
 
    override func validate() -> Bool
    {
        guard(self.owner != nil)
        else
        {
            return false
        }
        let ExtractedFromDb = self.owner?.task(withID:self.id())
        if(self.owner?.NULL_TASK()?.compareAttribute( ExtractedFromDb) == ComparisonResult.orderedSame)
        {
            self.owner?.addTask(withTask: self);
        }
        
        return true
    }

    override func deleteItem() {
        self.owner?.removeTask(withId: self.id())
    }


    override func request(withID RequestID: String!) -> KADomain!
    {
        var ReturnValue:KADomain? ;
        switch RequestID
        {
            case TaskGeneric.DEADLINE_ID:
                ReturnValue = self.deadline
                
            case TaskGeneric.NAME_ID:
                ReturnValue = KAGenericDomain.createGenericObject(withLabel: self.name!) as? KADomain
            
            case TaskGeneric.STATUS_ID:
                ReturnValue = self.status
            default:
                ReturnValue = super.request(withID: RequestID)
        }
        if(ReturnValue == nil)
        {
            ReturnValue = self
        }
        return ReturnValue!;
    }

    override func modifyChildDomain(withId Id: String!, withValue NewValue: KADomain!)
    {
        switch Id
        {
            case TaskGeneric.DEADLINE_ID:
                self.deadline = NewValue as? KADate
                
        
            case TaskGeneric.NAME_ID:
                self.name = NewValue.label();
            
            case TaskGeneric.STATUS_ID:
                self.status = NewValue as? KABoolean
        default: break
        }
       
    }
    
}

class NullTaskGeneric: KAGenericDomain,Task
{
    func taskId(TaskId ID: String) -> (String) {
        return super.id()
    }
    
    var name: String?
    {
        get
        {
            return super.label()
        }
        set{
            
        }
       
    }
    
    var deadline: KADate?
    
    var status: KABoolean?
    
    var owner: TaskDataBase?
    
    override init() {
        super .init(label: "NULL_TASK", withLabelIdentifier: "NULL_TASK", withObjectFamilyName: "NULL_TASK", withID: "NULL_TASK")
    }
}
