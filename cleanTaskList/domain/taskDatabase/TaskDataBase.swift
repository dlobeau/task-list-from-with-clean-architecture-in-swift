//
//  TaskDataBase.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 25/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework




protocol TaskDataBase:KADomain,DelegateTaskFactory
{
    
    var repository:Repository?{get set}
    
    func addTask(withTask NewTask:Task )
    func removeTask(withTask NewTask:Task )
    func removeTask(withId TaskId:String )
    func updateTask(withTask TaskToUpdate:Task )
    
    
    func task(withID ID:String)-> (Task?)
    func task(withIndex Index:Int)-> (Task?)
    func tasks() -> ([Task]?)
    func NULL_TASK() ->(Task?)
    
    func taskCount() ->(Int)
     
    var taskFactory:DelegateTaskFactory? {get set}
    

}

class TaskDataBaseSimpleComposite: KAGenericDomain,TaskDataBase
{
    var taskFactory: DelegateTaskFactory?
    var repository:Repository?
    
    lazy private var tasksObject = [String:Task]()
    
    
    
    static override func getTypeTag() -> String! {
        return "taskDataBaseGeneric"
    }
    
    static let NULL_TASK_OBJECT:Task = {
          NullTaskGeneric.init()
      }()
    
    func createNewTask(withName Name: String, WithDeadline Deadline: KADate) -> (Task?)
    {
        let ReturnValue = self.taskFactory?.createNewTask(withName:Name, WithDeadline:Deadline)
         ReturnValue!.owner = self;
        return ReturnValue
    }
    
    func tasks() -> ([Task]?)
      {
        return [Task](self.tasksObject.values)
      }
    
    func createNewTask() -> (Task?)
    {
        let ReturnValue = self.taskFactory?.createNewTask()
         ReturnValue!.owner = self;
        return ReturnValue
    }
    
    func addTask( withTask NewTask: Task)
    {
        let ID = self.createIDForNextInsertedTask();
        NewTask.setID( ID)
        self.tasksObject[NewTask.id()] = NewTask;
        //self.repository?.create(withItem: NewTask)
    }
    
    override func request(withID RequestID: String!) -> KADomain!
    {
        var ReturnValue:KADomain?
        
        switch RequestID {
        case "allTasks":
            ReturnValue = self.allTaskObject();
        default:
            ReturnValue = super.request(withID: RequestID)
        }
        return ReturnValue;
    }

    func allTaskObject()->(TaskList?)
    {
        let ReturnValue =  TaskListGeneric.createList() as! TaskList;
        
        let List = self.tasks();
        for currentTask in List!
        {
            ReturnValue.addTask(aTask: currentTask)
        }
        
        return ReturnValue;
    }
    

    func createIDForNextInsertedTask() ->(String)
    {
        return "task\(self.taskCount())"
    }
    
    func removeTask(withTask TaskToRemove: Task)
    {
        self.tasksObject.removeValue(forKey: TaskToRemove.id())
       // self.repository?.delete(withItem: TaskToRemove)
    }
    
    func removeTask(withId TaskId: String)
    {
        if let TaskToRemove = self.task(withID: TaskId)
        {
            self.removeTask(withTask: TaskToRemove)
        }
         
    }
    
    func updateTask(withTask TaskToUpdate:Task )
    {
        //self.repository?.update(withItem: TaskToUpdate)
    }
    
    func task(withID ID: String) -> (Task?)
    {
        if let ReturnValue = self.tasksObject[ID]
        {
            return ReturnValue
        }
        else
        {
            return self.NULL_TASK()
        }
        
    }
    
    func task(withIndex Index: Int) -> (Task?)
    {
        return self.tasks()![Index]
    }
    
    func NULL_TASK() -> (Task?)
    {
      return TaskDataBaseSimpleComposite.NULL_TASK_OBJECT
    }
    
    func taskCount() -> (Int) {
        return self.tasksObject.count
    }
    
 
    
    
    
}


