//
//  TaskFactory.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 30/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol DelegateTaskFactory
{
    func createNewTask(withName Name:String, WithDeadline Deadline:KADate) -> (Task?)
    func createNewTask() -> (Task?)
}

struct DelegateSimpleCompositeTaskFactory:DelegateTaskFactory
{
    func createNewTask(withName Name:String, WithDeadline Deadline:KADate) -> (Task?)
    {
        guard let App = KAApplicationSerializableObjectImp.instance()
        else {
            return TaskGeneric.init(WithName: Name, WithDeadLine: Deadline)
        }
        let ReturnValue:Task?
        ReturnValue = TaskGeneric.init(WithName: Name, WithDeadLine: Deadline)
        ReturnValue!.setFactoryDelegate(App.factory())
        return ReturnValue
    }
    
    func createNewTask() -> (Task?)
    {
        guard let App = KAApplicationSerializableObjectImp.instance()
        else {
            return NullTaskGeneric.init()
        }
        return self.createNewTask(withName: "Task", WithDeadline: (App.calendar()?.createCurrentDay())!)
    }
}
