//
//  DateSelectionPage.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 29/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol DateSelectionPage:KAPresenter,KASeriazableObject
{
    var datePicker:TLPicker?{get}
    var saveButton:TLButton?{get};
}

class DateSelectionPagePresenter:KAGenericPresenter,DateSelectionPage
{
    var datePicker: TLPicker?
    {
        return self.getChildwithIdentifier("datePicker") as? TLPicker
    }
    
    var saveButton: TLButton?
    {
        return self.getChildwithIdentifier("button") as? TLButton
    }
    
    static override func getTypeTag() -> String!
     {
        return "newDateWindow"
     }
    
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
        super.setTestdelegate(DateSelectionPagePresenterConformToDomain.init())
    }
       
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
           
    }
}

class DateSelectionPagePresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Page = Sender as! DateSelectionPage
        let DateReference = Domain as! KADate
        let datePicker = Page.datePicker!
        let DelegatePicker = datePicker.testdelegate?()
        DelegatePicker?.expectSender(datePicker, conformTo: DateReference)
        
    }
}
