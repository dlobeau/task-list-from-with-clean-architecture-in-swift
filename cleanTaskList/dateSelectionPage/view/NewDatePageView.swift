//
//  NewDatePageViewController.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 29/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class NewDatePageViewController: TLUIFacadeViewController
{
    
    @IBOutlet weak var saveButton: TLUISaveTaskButton!
    @IBOutlet weak var datePicker: TLUIDatePickerFacade!
   
    @IBAction func clickOnSaveButton(_ sender: Any)
    {
        self.datePicker.validateUserChange()
        self.interface()?.addCommandOnPresenterModificationWithPresenter(withNewValue: self.datePicker.interface()!, withSender: self.interface()!)
        self.interface()?.validateChange();
        self.closeWindow()
    }
    
   
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        self.interface().dataBinding()
        self.setContent()
    }
    
    override func setContent()
    {
        super.setContent()
        self.saveButton.setContent()
        self.datePicker.setContent()
        
    }
    
}
