//
//  SwiftInjDependencyInjection.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 09/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import Swinject

class ContenerDI: NSObject, KAInjectionDependencyDelegate
{
  
    let container = Container()
    
    override init()
    {
        super.init()
        setupDefaultContainers()
    }
    
    
    private func setupDefaultContainers()
    {
        let Parameter:[String:String] = ["group_id":"","identifier":"","label":"","type_id":""]
        
        container.register(TaskApplication.self, name: TaskApplicationComposite.getTypeTag(),
                           factory: { _ in TaskApplicationComposite(dictionary: Parameter) })
         
        container.register(TaskDataBase.self, name: TaskDataBaseSimpleComposite.getTypeTag(),
                           factory: { _ in TaskDataBaseSimpleComposite(dictionary: Parameter) })
        
        container.register(TaskListWindow.self, name: TaskListWindowPresenter.getTypeTag(),
                           factory: { _ in TaskListWindowPresenter(dictionary: Parameter) })
        
        container.register(TasksListTable.self, name: TasksListTablePresenter.getTypeTag(),
                           factory: { _ in TasksListTablePresenter(dictionary: Parameter) })
        
        container.register(TasksListSection.self, name: TaskListSectionPresenter.getTypeTag(),
                           factory: { _ in TaskListSectionPresenter(dictionary: Parameter) })
        
        container.register(TasksListTableCell.self, name: TasksListTableCellPresenter.getTypeTag(),
                           factory: { _ in TasksListTableCellPresenter(dictionary: Parameter) })
        
        container.register(QueryBusinessUseCaseAllItem.self, name: QueryBusinessUseCaseAllItemFromDB.getTypeTag(),
                           factory: { _ in QueryBusinessUseCaseAllItemFromDB(dictionary: Parameter)})
        
        container.register(QueryBusinessUseCaseNewTask.self, name: QueryBusinessUseCaseNewTaskFromDB.getTypeTag(),
                           factory: { _ in QueryBusinessUseCaseNewTaskFromDB(dictionary: Parameter)})
        
        container.register(TaskDescriptionPage.self, name: TaskDescriptionPagePresenter.getTypeTag(),
                           factory: { _ in TaskDescriptionPagePresenter(dictionary: Parameter)})
        
        container.register(TaskNameEditBox.self, name: TaskNameEditBoxPresenter.getTypeTag(),
                           factory: { _ in TaskNameEditBoxPresenter(dictionary: Parameter)})
        
        container.register(DateSelectionPage.self, name: DateSelectionPagePresenter.getTypeTag(),
                           factory: { _ in DateSelectionPagePresenter(dictionary: Parameter)})
        
        container.register(TaskListTableCellCheckBox.self, name: TaskListTableCellCheckBoxPresenter.getTypeTag(),
                           factory: { _ in TaskListTableCellCheckBoxPresenter(dictionary: Parameter)})
        
        container.register(Task.self, name: TaskGeneric.getTypeTag(),
        factory: { _ in TaskGeneric(dictionary: Parameter)})
        
    }
    
    
    func injectedObject(withParameters Parameters: [AnyHashable : Any]) -> Any
    {
        var ReturnValue:KASeriazableObject?
        
        if let Name = Parameters["group_id"] as? String
        {
            switch Name
            {
                case TaskApplicationComposite.getTypeTag():
                    ReturnValue = container.resolve(TaskApplication.self, name: TaskApplicationComposite.getTypeTag()) as? KASeriazableObject
                     
                case TaskDataBaseSimpleComposite.getTypeTag():
                    ReturnValue = container.resolve(TaskDataBase.self, name: TaskDataBaseSimpleComposite.getTypeTag())
                
                case TaskListWindowPresenter.getTypeTag():
                ReturnValue = container.resolve(TaskListWindow.self, name: TaskListWindowPresenter.getTypeTag())
                
                case TasksListTablePresenter.getTypeTag():
                 ReturnValue = container.resolve(TasksListTable.self, name: TasksListTablePresenter.getTypeTag())
                
                case TaskListSectionPresenter.getTypeTag():
                ReturnValue = container.resolve(TasksListSection.self, name: TaskListSectionPresenter.getTypeTag())
                
                case TasksListTableCellPresenter.getTypeTag():
                               ReturnValue = container.resolve(TasksListTableCell.self, name: TasksListTableCellPresenter.getTypeTag())
                
                case QueryBusinessUseCaseAllItemFromDB.getTypeTag():
                                ReturnValue = container.resolve(QueryBusinessUseCaseAllItem.self, name: QueryBusinessUseCaseAllItemFromDB.getTypeTag())
                
                case QueryBusinessUseCaseNewTaskFromDB.getTypeTag():
                ReturnValue = container.resolve(QueryBusinessUseCaseNewTask.self, name: QueryBusinessUseCaseNewTaskFromDB.getTypeTag())
                
                case TaskDescriptionPagePresenter.getTypeTag():
                ReturnValue = container.resolve(TaskDescriptionPage.self, name: TaskDescriptionPagePresenter.getTypeTag())
                
                case TaskNameEditBoxPresenter.getTypeTag():
                ReturnValue = container.resolve(TaskNameEditBox.self, name: TaskNameEditBoxPresenter.getTypeTag())
                
                case DateSelectionPagePresenter.getTypeTag():
                ReturnValue = container.resolve(DateSelectionPage.self, name: DateSelectionPagePresenter.getTypeTag())
                
                case TaskListTableCellCheckBoxPresenter.getTypeTag():
                ReturnValue = container.resolve(TaskListTableCellCheckBox.self, name: TaskListTableCellCheckBoxPresenter.getTypeTag())
                
                default:
                    ReturnValue = KAApplicationSerializableObjectImp.injectionDependencyDelegateDefaultItems()?.injectedObject(withParameters: Parameters) as? KASeriazableObject
            }
            ReturnValue?.setObjectFamilyName(Parameters["group_id"] as! String)
            ReturnValue?.setID(Parameters["type_id"] as! String)
            ReturnValue?.setLabel(Parameters["label"] as! String)
            ReturnValue?.setLabelIdentifier(Parameters["identifier"] as! String)
            
        }
        return ReturnValue!
    }
    
}
