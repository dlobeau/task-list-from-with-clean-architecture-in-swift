//
//  TaskApplicationManager.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 24/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import CoreData

protocol TaskApplication:KAApplication
{
    var taskDataBase:TaskDataBase?{get}
    
}

class TaskApplicationComposite :KAApplicationSerializableObjectImp,
    TaskApplication
{
    var repository: Repository?
    
    static override func getTypeTag() -> String!
    {
        return "mainApplication"
    }
    
   
    
    lazy var taskDataBase:TaskDataBase? =
        {
            var ReturnValue:TaskDataBase?
            let Extract = self.datBaseContener()?.request(withID: "taskDataBase")
            
           
            
            if let DataBase = Extract  as? TaskDataBase
            {
                DataBase.repository = RepositoryCoreData.init()
                ReturnValue = DataBase
            }
            else
            {
                ReturnValue  =  nil
            }
            return ReturnValue
        }()
    
    
    override func initializaDataBase()
    {
        self.taskDataBase?.taskFactory =  DelegateSimpleCompositeTaskFactory.init()
        
    }
    
    override func start()
    {
        super.start()
        
        //databinding for UITest
        let environment = ProcessInfo.processInfo.environment
        var DataBinding:KADomain?
        
        if let DataBindingString = environment ["dataBinding"]
        {
            DataBinding = self.factory().createAttribute(from: DataBindingString) as? KADomain;
        }
        self.window()?.setData(DataBinding);
        //end UITest DataBinding
    }
    
    
     
}



 
 



