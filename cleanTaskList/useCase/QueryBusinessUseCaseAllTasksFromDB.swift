//
//  QueryBusinessUseCaseAllTasksFromDB.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 22/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol QueryBusinessUseCaseAllItem:KAQueryBusinessUseCase,KASeriazableObject
{
}


class QueryBusinessUseCaseAllItemFromDB:KASeriazableObjectTableImp,QueryBusinessUseCaseAllItem
{
    static override func getTypeTag() -> String!
    {
        return "useCaseAllItemFromDB"
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!)
    {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
    }
      
    
    func execute(withRepository Repository: KADomain!, withValue Value: KADomain!) -> KADomain!
    {
        let DataBaseLink = Repository as? KADomainLink
        let DataBase = DataBaseLink?.source() as? TaskDataBase;
        
        return DataBase!.request(withID:"allTasks") 
    }
    
    
}

