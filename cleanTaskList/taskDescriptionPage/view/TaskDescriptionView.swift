//
//  TaskDescriptionView.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 28/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class TaskDescriptionViewControler:TLUIFacadeViewController
{
    @IBOutlet weak var deadlineButton: TLUIButtonFacade!
    @IBOutlet weak var editBoxName: TLUITextFieldFacade!
    @IBOutlet weak var editBoxLabel: TLUIStaticLabel!
    @IBOutlet weak var saveButton: TLUIButtonFacade!
    
    override func viewWillAppear(_ animated: Bool)
     {
         super.viewWillAppear(animated);
         self.setContent()
     }
    
    override func setContent()
    {
        super.setContent()
        self.deadlineButton.setContent()
        self.editBoxName.setContent()
        self.editBoxLabel.setContent()
        self.saveButton.setContent()
    }
    
    @IBAction func clickOnSaveButton(_ sender: Any)
    {
        self.interface()?.addCommand(onChildPresenterModification: self.editBoxName.interface(),
                                     withSender: self.interface())
        self.interface()?.addCommand(onChildPresenterModification: self.deadlineButton.interface(),
                                     withSender: self.interface())
        self.interface()?.executeCommandHierarchy()
        
        let SaveEvent =  self.saveButton!.interface()?.event?()
        SaveEvent?.doAction(withSender: self.saveButton.interface())
       
        self.closeWindow()
    }
   
    @IBAction func clickOnDeadlineButton(_ sender: KAView)
    {
        let E = sender.interface()?.event?()
        let NewWindowPresenter = E!.getDestinationWithOwner(sender.interface())
        let NewWindow = NewWindowPresenter?.createView(withOwner: sender)
        self.delegatePushNextView()?.pushNextPage(NewWindow)
    }
}
