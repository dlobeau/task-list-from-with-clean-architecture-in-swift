//
//  taskDescriptionPage.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 27/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import  cleanProjectFramework


protocol TaskDescriptionPage: KAPresenter,KASeriazableObject
{
    var editBoxName:TaskNameEditBox?{get}
    var deadlineSelectionButton:TLButton?{get}
    var saveButton:TLButton?{get}

}

class TaskDescriptionPagePresenter: KAGenericPresenter,TaskDescriptionPage
{
    var deadlineSelectionButton: TLButton?
    {
        get
        {
            return self.getChildwithIdentifier("deadlineButton") as? TLButton
        }
    }
    var editBoxName:TaskNameEditBox?
    {
        get
        {
            return self.getChildwithIdentifier("editBoxWithName") as? TaskNameEditBox
        }
    }
    var saveButton: TLButton?
    {
        get
        {
            return self.getChildwithIdentifier("button") as? TLButton
        }
    }
    
    static override func getTypeTag() -> String!
    {
              return "taskDetailsWindow"
    }
      
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
           super.init(dictionary: Dictionary)
        super.setTestdelegate(TaskDescriptionPagePresenterPresenterConformToDomain.init())
    }
       
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
           
          
    }
}


class TaskDescriptionPagePresenterPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Page = Sender as! TaskDescriptionPage
        let ReferenceTask = Domain as! Task
        let EditBOx = Page.editBoxName
        let DelegateEditBoxTest = EditBOx!.testdelegate?()
        DelegateEditBoxTest?.expectSender?(Page.editBoxName!, conformToText: ReferenceTask.name!)
        
        let DelegateDeadLine = Page.deadlineSelectionButton?.testdelegate?()
        DelegateDeadLine?.expectSender?(Page.deadlineSelectionButton!, conformToText: ReferenceTask.deadline!.completeString())
        
        
    }
}
