//
//  TaskNameEditBox.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 28/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol TaskNameEditBox:KAPresenter,KASeriazableObject
{
    
}

class TaskNameEditBoxPresenter:KAGenericPresenter,TaskNameEditBox
{
    override static func getTypeTag() -> String!
    {
        return "editBoxWithName"
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
              super.init(dictionary: Dictionary)
           super.setTestdelegate(TaskNameEditBoxPresenterTest.init())
            super.setViewFactoryDelegate(KACreateUIWidgetEditBoxFactory.init())
       }
          
       override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
       {
              super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
                   
       }
}

class TaskNameEditBoxPresenterTest:NSObject,KAPresenterConformToDomain
{
    
    
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
    }
    
    func expectSender(_ Sender: KAPresenter, conformToText ReferenceText: String)
    {
        let TextUnderTest = Sender.getText?()
        assert(ReferenceText == TextUnderTest, "\(TextUnderTest!) instead of \(ReferenceText)")
        
    }
}
