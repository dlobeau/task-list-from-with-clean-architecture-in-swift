//
//  KACalendarObjectSerializable.m
//  kakebo
//
//  Created by Didier Lobeau on 27/02/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACalendarGeneric.h"

#import "KADateAttribute.h"


#import "KANullDateGeneric.h"


@interface KACalendarGeneric()


@property  NSDateFormatter * dateFormat ;
@property id<KADate> NULL_DATE;

@property NSLocale *usedLocal;
@end


@implementation KACalendarGeneric

@synthesize  dateFormat = _dateFormat ;
@synthesize  usedLocal = _usedLocal ;
@synthesize  NULL_DATE = _NULL_DATE;


+(NSString *) getTypeTag
{
    return @"calendar";
}

-(NSLocale *) usedLocal
{
    if(_usedLocal == nil)
    {
        _usedLocal = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    }
    return _usedLocal;
}
-(void) setUsedLocal:(NSLocale *)usedLocal
{
    _usedLocal = usedLocal;
}



-(id<KADate>) createCurrentDay
{
    return [self createDayWithDateObject:[NSDate date]];
   
}

-(id<KADate>) createDayWithString:(NSString *) strDay
{
    id<KADate> Date = [[KADateAttribute alloc] initWithDateString:strDay WithTypeId:@"date"];
    
    Date.calendar = self;
    return Date;
    
}

-(id<KADate>) createDayWithDateObject:(NSDate *) DateObject
{
    NSString * strDate =[[self getDateFormat] stringFromDate:DateObject];
    id<KADate> Date = [[KADateAttribute alloc] initWithDateString:strDate WithTypeId:@"date"];
    
    Date.calendar = self;
    
    return Date;
}

-  (NSDateFormatter *) getDateFormat
{
    if( _dateFormat == nil )
    {
        _dateFormat = [[NSDateFormatter alloc] init];
        _dateFormat.locale = self.usedLocal;
        [_dateFormat setDateFormat:@"yyyy-MM-dd"];
        
    }
    return _dateFormat;
}

-(id<KADate>) NULL_DATE
{
    if(_NULL_DATE == nil)
    {
        _NULL_DATE = [[KANullDateGeneric alloc] init];
    }
    return _NULL_DATE;
}
-(void) setNULL_DATE:(id<KADate>)NULL_DATE
{
    _NULL_DATE = NULL_DATE;
}
@end
