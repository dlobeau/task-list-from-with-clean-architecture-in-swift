//
//  KANullDateGeneric.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KADateAttribute.h"

NS_ASSUME_NONNULL_BEGIN

@interface KANullDateGeneric : KADateAttribute

@end

NS_ASSUME_NONNULL_END
