//
//  KACalendarObjectSerializable.h
//  kakebo
//
//  Created by Didier Lobeau on 27/02/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"
#import "KACalendar.h"

@interface KACalendarGeneric : KAGenericDomain<KACalendar>

@end
