//
//  KACriteriaSerializableObjectImp.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KACriteria.h"
#import "KASeriazableObjectTableImp.h"

@interface KACriteriaSerializableObjectImp : KASeriazableObjectTableImp<KACriteria>



@end
