//
//  KAUINavigationBar.h
//  kakebo
//
//  Created by Didier Lobeau on 17/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAView.h"
#import "KADelegatePushNextView.h"

@interface TLUINavigationController : UINavigationController <KAView,KADelegatePushNextView>

-(id<KAView>) mainRootWindow;

@end
