//
//  KAUITableView.m
//  kakebo
//
//  Created by Didier Lobeau on 19/04/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAUITableView.h"
#import "KASection.h"
#import "KACell.h"
#import "KACellSwipeAction.h"
#import "KAEvent.h"
#import "KADelegatePushNextView.h"
#import "KATable.h"
#import "TLUITableViewCellFacade.h"
@implementation KAUITableView


-(void) registerSectionHeaderCellsWithIDList:(NSSet<NSString *> *)IDList
{
    NSEnumerator<NSString *> *enumerator = [IDList objectEnumerator];
    NSString * value;
    
    while ((value = [enumerator nextObject]))
    {
        UINib *Nib = [UINib nibWithNibName:value bundle:nil];
        
        [self registerNib:Nib forHeaderFooterViewReuseIdentifier:value];
    }
}



#pragma UITableViewDataSource  delegate #######################

- (nonnull KAUITableView *)tableView:(nonnull KAUITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    KAUITableView *ReturnValue = nil;
    
    id<KASection> SectionPresenter = [self.interface sectionAtIndex:indexPath.section];
    id<KACell> CellPresenter = [SectionPresenter cellAtIndex:indexPath.row] ;
    
    ReturnValue = (KAUITableView *)[CellPresenter createViewWithOwner:tableView];
   
    [ReturnValue setContent];
    return ReturnValue;
}

#pragma UITableViewDelegate delegate #######################

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.interface sectionAtIndex:section] cells] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.interface.sections.count;;
}

- (void)tableView:(KAUITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //TLUITableViewCellFacade *Cell = [tableView cellForRowAtIndexPath:indexPath];
   // [Cell clickWithSender:tableView];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
     id<KASection> CellContener = [self.interface sectionAtIndex:indexPath.section];
     id<KACell> CellPresenter = [CellContener cellAtIndex:indexPath.row] ;
     BOOL Value = (CellPresenter.swipLeftEvents.count >0);
    return Value;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray<UITableViewRowAction *> *ReturnValue = nil;
    id<KASection> CellContener = [self.interface sectionAtIndex:indexPath.section];
    id<KACell> CellPresenter = [CellContener cellAtIndex:indexPath.row] ;
    
    for(int i = 0; i< CellPresenter.swipLeftEvents.count; i++)
    {
        id<KACellSwipeAction> CurrentAction = [CellPresenter.swipLeftEvents objectAtIndex:i];
        UITableViewRowAction *Action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:CurrentAction.getText handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            [self SwipeActionWithEvent:CurrentAction.event WithSender:[tableView cellForRowAtIndexPath:indexPath]];
            
        }];
        if(ReturnValue == nil)
        {
            ReturnValue = [[NSMutableArray alloc] init];
        }
        [ReturnValue addObject:Action];
    }
    return ReturnValue;
}

-(void) SwipeActionWithEvent:(id<KAEvent>) Event WithSender:(id<KAView>) Sender
{
    if( Event != nil )
    {
        id<KAPresenter> NewWindowPresenter = [Event getDestinationWithOwner:Sender.interface];
        if(NewWindowPresenter != nil)
        {
            id<KAView>NewWindow = [NewWindowPresenter createViewWithOwner:Sender];
            if(NewWindow != nil)
            {
                [Sender.delegatePushNextView pushNextPage:NewWindow];
            }
        }
    }
}
@end
