//
//  KACreateUIWidgetEditBoxFactory.h
//  taskList
//
//  Created by Didier Lobeau on 10/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACreateUIWidgetGenericFactory.h"

NS_ASSUME_NONNULL_BEGIN

@interface KACreateUIWidgetEditBoxFactory : KACreateUIWidgetGenericFactory

@end

NS_ASSUME_NONNULL_END
