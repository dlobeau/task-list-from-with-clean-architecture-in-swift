//
//  UIView(KAKEBO).m
//  kakebo
//
//  Created by Didier Lobeau on 01/04/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "UIView+KAKEBO.h"
#import "KAView.h"

@implementation UIView (KAKEBO)

-(UIView *) getChildWithID:(NSString *) ID
{
    UIView * returnValue = nil;
    NSArray *ViewList = [self subviews];
    if([ViewList count] == 1  )
    {
        UIView *V = [ViewList objectAtIndex:0];
        if([V IsStackViewType])
        {
            ViewList = [V subviews];
        }
    }
    returnValue = [self getWidgetWithID:ID InList:ViewList];
    
    return returnValue;
    
}

-(UIView *) getWidgetWithID:(NSString *) ID InList:(NSArray<UIView<KAView> *> *) ViewList
{
    BOOL bfound = NO;
    UIView * ReturnValue = nil;
    for(int i = 0; !bfound && (i < [ViewList count]); i++ )
    {
        UIView<KAView>* currentView = (UIView<KAView> *)[ViewList objectAtIndex:i];
        if([currentView IsStackViewType])
        {
            NSArray *ViewList2 = [(UIStackView *)currentView subviews];
            if([ViewList2 count] >= 1)
            {
                currentView = (UIView<KAView>* ) [self getWidgetWithID:ID InList:ViewList2];
                if(currentView != nil)
                {
                    ReturnValue = (UIView *)currentView;
                    bfound = YES;
                }
            }
        }
        else if([currentView conformsToProtocol:@protocol(KAView)])
        {
            NSString *ViewID = [(id<KAView>)currentView widgetID];
            
            if([ViewID isEqual:ID])
            {
                bfound = YES;
                ReturnValue = (UIView *)currentView;
            }
        }
    }
    return ReturnValue;
}

-(BOOL) IsStackViewType
{
    return [self isKindOfClass:[UIStackView class]];
}


-(NSArray<UIView *> *) getChildList
{
    NSMutableArray<UIView *> *ReturnValue = nil;
    
    NSArray *ViewList = [self subviews];
    for(int i = 0;  (i < [ViewList count]); i++ )
    {
        UIView* currentView = [ViewList objectAtIndex:i];
        if([currentView IsStackViewType])
        {
            NSArray *ViewList = [currentView getChildList];
            if(ViewList.count >0)
            {
                if(ReturnValue == nil)
                {
                    ReturnValue = [[NSMutableArray alloc] init];
                }
                [ReturnValue addObjectsFromArray:ViewList];
            }
        }
        else if([currentView conformsToProtocol:@protocol(KAView)])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:currentView];
        }
        
    }
    
    return ReturnValue;
}






@end
