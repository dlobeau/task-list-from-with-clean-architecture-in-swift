//
//  createUIWidgetStoroyboqrdFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 07/10/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KACreateUIWidgetFactory.h"

@interface KACreateUIWidgetGenericFactory : NSObject<KACreateUIWidgetFactory>


-(BOOL) isWidgetTypeValid:(NSString *) WidgetId;

-(BOOL) isWidgetIdentifierValid:(NSString *) identifier WithWidgetID:(NSString *) WidgetID;

@end
