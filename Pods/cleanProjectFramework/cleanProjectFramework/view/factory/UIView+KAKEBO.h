//
//  UIView(KAKEBO).h
//  kakebo
//
//  Created by Didier Lobeau on 01/04/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KAView;

@interface UIView (KAKEBO)

-(id<KAView>) getChildWithID:(NSString *) ID ;
-(NSArray<id<KAView>> *) getChildList ;

@end

NS_ASSUME_NONNULL_END
