//
//  KACreateWindowFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 15/11/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACreateUIWidgetGenericFactory.h"

@interface KACreateWindowFactory : KACreateUIWidgetGenericFactory

@end
