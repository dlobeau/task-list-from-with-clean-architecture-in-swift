//
//  KADelegatePushNextView.h
//  kakebo
//
//  Created by Didier Lobeau on 25/07/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//



@class KAView;;

@protocol KADelegatePushNextView 

-(void) pushNextPage:(id<KAView>) nextPage;

@end
