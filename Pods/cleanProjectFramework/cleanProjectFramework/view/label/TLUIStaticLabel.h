//
//  TLUIStaticLabel.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLUILabelFacade.h"

NS_ASSUME_NONNULL_BEGIN

@interface TLUIStaticLabel : TLUILabelFacade

@end

NS_ASSUME_NONNULL_END
