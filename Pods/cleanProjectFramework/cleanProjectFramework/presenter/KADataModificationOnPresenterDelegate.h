//
//  KADataModificationOnPresenterDelegate.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

@protocol KAPresenter;

NS_ASSUME_NONNULL_BEGIN

@protocol KADataModificationOnPresenterDelegate

-(void) modificationOccuredOnObservedPresenterWithSender:(id<KAPresenter>) Sender;

@end

NS_ASSUME_NONNULL_END
