//
//  KAGenericPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"
#import "KASeriazableObjectTableImp.h"

NS_ASSUME_NONNULL_BEGIN

@interface KAGenericPresenter : KASeriazableObjectTableImp<KAPresenter>

@end

NS_ASSUME_NONNULL_END
