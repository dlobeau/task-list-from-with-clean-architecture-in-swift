//
//  KCommandChangeDate.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KCommandChangeDate.h"
#import "KAApplicationSerializableObjectImp.h"
#import "KACalendar.h"
#import "KADate.h"

@interface KCommandChangeDate()

@property  NSDate * dateNewValue;

@end

@implementation KCommandChangeDate

-(id) initWithOwner:(id<KAPresenter>)Owner WithNewDate:(nonnull NSDate *)NewDate
{
    self = [super initWithOwner:Owner];
    
    self.dateNewValue = [NewDate copy];
    
    return self;
}

-(void) doCommand
{
    id<KACalendar> Calendar = [[KAApplicationSerializableObjectImp instance] calendar];
    NSAssert([Calendar conformsToProtocol:@protocol(KACalendar)], @"Calendar should be defined in application before calling KCommandChangeDate");
    
    id<KADate> NewDate = [Calendar createDayWithDateObject:self.dateNewValue];
    
    [self.owner setData:NewDate];
}

@end
