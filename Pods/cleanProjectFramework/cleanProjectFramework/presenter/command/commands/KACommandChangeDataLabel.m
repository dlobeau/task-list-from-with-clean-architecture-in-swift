//
//  KACommandChangeDataLabel.m
//  kakebo
//
//  Created by Didier Lobeau on 14/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KACommandChangeDataLabel.h"
#import "KADomain.h"
#import "KAPresenter.h"
#import "KADomainLink.h"

@interface KACommandChangeDataLabel()

    @property  NSString * labelNewValue;

@end

@implementation KACommandChangeDataLabel




-(id) initWithOwner:(id<KAPresenter>)Owner WithNewLabel:(NSString *)NewLabel
{
    self = [super initWithOwner:Owner];
   
    self.labelNewValue = [NewLabel copy];
    
    return self;
}

-(void) doCommand
{
    id<KADomain> Data = self.owner.data;
    if([Data conformsToProtocol:@protocol(KADomainLink)])
    {
        Data = [(id<KADomainLink>)Data source];
    }
    
    [Data  setLabel:self.labelNewValue];
}


@end
