//
//  KACommand.h
//  kakebo
//
//  Created by Didier Lobeau on 13/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KADomain;
@protocol KAPresenter;

@protocol KACommand <NSObject>

-(void) doCommand;


@end
