//
//  TLBarButton.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLButton.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLBarButton <TLButton>

@end

NS_ASSUME_NONNULL_END
