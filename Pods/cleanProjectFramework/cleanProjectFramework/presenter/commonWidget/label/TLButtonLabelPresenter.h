//
//  TLButtonLabelPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KALabelPresenter.h"
#import "TLButtonLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLButtonLabelPresenter : KALabelPresenter<TLButtonLabel>

@end

NS_ASSUME_NONNULL_END
