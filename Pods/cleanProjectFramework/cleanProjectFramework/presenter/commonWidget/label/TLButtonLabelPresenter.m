//
//  TLButtonLabelPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLButtonLabelPresenter.h"

@implementation TLButtonLabelPresenter

+(NSString *) getTypeTag
{
    return @"buttonLabel";
}

-(BOOL) isInitializedOnParentCreation
{
    return NO;
}

@end
