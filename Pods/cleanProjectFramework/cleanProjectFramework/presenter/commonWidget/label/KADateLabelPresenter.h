//
//  KADateLabelPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KALabelPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@interface KADateLabelPresenter : KALabelPresenter

@end

@interface KADateLabelPresenterConformToDomain:KALabelPresenterPresenterConformToDomain


@end

NS_ASSUME_NONNULL_END
