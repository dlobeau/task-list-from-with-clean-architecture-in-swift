//
//  KAStaticLabelPresenter.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KALabelPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@interface KAStaticLabelPresenter : KALabelPresenter

@end

NS_ASSUME_NONNULL_END
