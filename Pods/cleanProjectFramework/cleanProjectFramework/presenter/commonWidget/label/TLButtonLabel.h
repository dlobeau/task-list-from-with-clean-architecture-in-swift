//
//  TLButtonLabel.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KALabel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLButtonLabel <KALabel>

@end

NS_ASSUME_NONNULL_END
