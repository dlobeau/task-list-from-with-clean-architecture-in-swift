//
//  KACellSwipActionPresenter.h
//  kakebo
//
//  Created by Didier Lobeau on 28/02/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KACellSwipeAction.h"
NS_ASSUME_NONNULL_BEGIN

@interface KACellSwipActionPresenter : KAGenericPresenter<KACellSwipeAction>

@end

NS_ASSUME_NONNULL_END
