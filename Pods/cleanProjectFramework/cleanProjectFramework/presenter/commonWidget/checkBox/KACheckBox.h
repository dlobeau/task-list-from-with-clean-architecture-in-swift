//
//  KACheckBox.h
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"
#import "KABoolean.h"
NS_ASSUME_NONNULL_BEGIN

@protocol KACheckBox <KAPresenter>


-(BOOL) value;

@end

NS_ASSUME_NONNULL_END
