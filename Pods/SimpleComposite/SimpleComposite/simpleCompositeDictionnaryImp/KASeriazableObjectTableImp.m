//
//  KAGenericAttributeContener.m
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectTableImp.h"
#include "KABasicFile.h"
#import "KASerializeObjectLink.h"
#import "NSString+KASeriazableObject.h"
#import "KASeriazableObjectImpLink.h"
#import "KATableFifoChildsManagement.h"
#import "KASerializeObjectFactory.h"

@interface KASeriazableObjectTableImp()

@property id<KASeriazableObjectTable> additionalDataForIteration;
@property id<KATableChildsManagementDelegate> childsDelegate;


@end

@implementation KASeriazableObjectTableImp

@synthesize childsDelegate = _childsDelegate;

+(NSString *)getTypeTag
{
    return @"serilizableObjectTable";
}

static NSString *strMultiSelectedAllowAttributeTag = @"multiSelectedAllow";
static NSString *strSelectedAttributeTag = @"selected";

+(NSString *) getMutiSelectedAllowAttributeTypeTag
{
    return strMultiSelectedAllowAttributeTag;
}

+(NSString *) getSelectedAttributeTypeTag
{
    return strSelectedAttributeTag;
}





- (NSUInteger) count
{
    return [self.childsDelegate count];
}

-(void) addInChildContenerObject:(id<KASeriazableObject>) Object
{
    
    [self.childsDelegate addInChildContenerObject:Object];
}

-(BOOL) emptyChildList
{
    return [self.childsDelegate emptyChildList];
}

-(BOOL) removeChild:(KASeriazableObjectImp * ) ChildObject
{
    return [self.childsDelegate removeChild:ChildObject];
}

-(void) removeObjectAtIndex:(NSInteger) Index
{
    [self.childsDelegate removeObjectAtIndex:Index];
}

-(NSArray<id<KASeriazableObject>> *) childs
{
    return [self.childsDelegate childs];
}

-(BOOL) changeChildAtRank:(NSInteger) rank WithValue:(id<KASeriazableObject>) Value
{
   
    return [self.childsDelegate changeChildAtRank:rank WithValue:Value];
}



-(id<KATableChildsManagementDelegate>) childsDelegate
{
    if(_childsDelegate == nil)
    {
        _childsDelegate = [[KATableFifoChildsManagement alloc] initWithSender:self];
    }
    return _childsDelegate;
}
-(void) setChildsDelegate:(id<KATableChildsManagementDelegate>) childsDelegate
{
    _childsDelegate = childsDelegate;
}

-(id<KAIterator>) getIterator
{
    return self.childsDelegate.getIterator;
}
-(BOOL) addAllChild:(NSArray<id<KASeriazableObject>> *) ChildListObject
{
    BOOL bReturnValue = YES;
    
    if(ChildListObject != nil)
    {
        for(int i = 0; bReturnValue && (i < [ChildListObject count]);i++)
        {
            bReturnValue = [self addChild:[ChildListObject objectAtIndex:i]];
        }
    }
    return bReturnValue;
}

-(BOOL) addChild:(id<KASeriazableObject> ) ChildObject
{
    return [self addChild:ChildObject WithTypeId:ChildObject.ID];
}

-(BOOL) addChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) TypeId
{
    BOOL bReturnValue = YES;
    @synchronized (self)
    {
        ChildObject.ID = TypeId;
    
        NSAssert([self isInsertionOkForChild:ChildObject],@"could not add child %@ to contener: %@ , contener's rules broken",[ChildObject attributeIdentity],[self attributeIdentity]  );
    
           NSInteger index = [self foundWithTypeId:[ChildObject ID]];
        
        if( index== -1 )
        {
            [self addInChildContenerObject:ChildObject];;
        }
        
        else
        {
            bReturnValue = [self changeChildAtRank:index WithValue:ChildObject];
            
        }
       
    }
    return bReturnValue;
}

-(BOOL) removeAllChild:(NSArray<id<KASeriazableObject>>* ) ChildObjectList
{
    for(int i = 0; (ChildObjectList != nil) && (i < [ChildObjectList count]); i++)
    {
        id<KASeriazableObject> currentObject = [ChildObjectList objectAtIndex:i];
        [self removeChild:currentObject];
    }
    
    return NO;
}

-(void) setSelfAsParentWithObject:(id<KASeriazableObject>) Object
{
    [Object setParent:self];
}

-(BOOL) isInsertionOkForChild:(id<KASeriazableObject>) Child
{
    return ![self isObjectInGenealogyTree:Child];
}

-(BOOL) isObjectInGenealogyTree:(id<KASeriazableObject>) Object
{
    BOOL bReturnValue = NO;
    
    id<KASeriazableObject> Parent = self;
    
    while((Parent != nil) && !bReturnValue)
    {
        if(Parent == Object)
        {
            bReturnValue = YES;
            
        }
        Parent = [Parent parent];
        
    }
    
    return bReturnValue;
}

-(BOOL) addCloneOfChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id
{
    id<KASeriazableObject> NewObject = [ChildObject cloneObject];
    NewObject.ID =Type_id;
    
    return [self addChild:NewObject];
}

-(BOOL) addLinkToChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id
{
    return [self addChild:[self.factoryDelegate createFromSource:ChildObject WithID:Type_id]];;
}











-(id<KASeriazableObject>) objectAtIndex:(NSInteger) Index
{
    id<KASeriazableObject> ReturnValue = nil;
    @synchronized (self)
    {
      
          
        id<KAIterator> Iterator = [self getIterator];
        for(int i = 0; [Iterator hasNext] && (i<  Index) ;i++)
        {
            [Iterator next];
        }
        
        if([Iterator hasNext])
        {
            ReturnValue = [Iterator next];
        }
        
    }
    return ReturnValue;
}




-(NSInteger) foundWithTypeId:(NSString *) TypeId
{
    NSInteger ReturnValue = -1;
    BOOL bFound = NO;
    NSInteger i = 0;
    id<KAIterator> Iterator = [self getIterator];
    while([Iterator hasNext] && !bFound)
    {
        id<KASeriazableObject> currentAttribute = [Iterator next];
        
        if( [[currentAttribute ID] isEqual:TypeId])
        {
            ReturnValue = i;
            bFound = YES;
        }
        i++;
    }
    
    return ReturnValue;
}

-(id<KASeriazableObject> ) getChildwithGroupId:(NSString *) GroupId WithTypeId:(NSString *) TypeId
{
    id<KASeriazableObject> ReturnValue;
    
    BOOL bFound = false;
    
    id<KAIterator> Iterator = [self getIterator];
    while([Iterator hasNext])
    {
        id<KASeriazableObject> currentAttribute = [Iterator next];
        
        if( [[currentAttribute objectFamilyName] isEqual:GroupId] && [[currentAttribute ID] isEqual:TypeId])
        {
            ReturnValue = currentAttribute;
            bFound = YES;
        }
    }
    return ReturnValue;
}


















-(NSArray<id<KASeriazableObject>> *) getChildListWithGroupId:(NSString *) GroupId
{
    NSMutableArray* ReturnValue;
    id<KASeriazableObject> SourceNode = self;
    NSString * ResearchTag = GroupId;
    
    NSDictionary<NSString *,NSString *> *SplitedInformation = [ResearchTag splitWithPathAndFamillyName];
    if(SplitedInformation != nil)
    {
        SourceNode = [self getChildwithTypeId:[SplitedInformation objectForKey:[NSString pathTag]]];
        if(SourceNode != nil)
        {
            ResearchTag = [SplitedInformation objectForKey:[NSString objectFamilyNameTag]];
        }
    }
    
    id<KAIterator> Iterator = [SourceNode getIterator];
    while([Iterator hasNext])
    {
        id<KASeriazableObject> currentAttribute = [Iterator next];
        
        if( [currentAttribute.objectFamilyName isEqual:ResearchTag])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            if([currentAttribute conformsToProtocol:@protocol(KASerializeObjectLink)])
            {
                currentAttribute = [(id<KASerializeObjectLink>)currentAttribute source];
            }
            
            [ReturnValue addObject:currentAttribute];
        }
        
    }
    return ReturnValue;
}




-(id<KASeriazableObject,KATreeNavigable> ) getChildwithTypeId:(NSString *) TypeId
{
    return [self getChildwithTypeId:TypeId WithDynamicallyCreatedChildsInclude:NO];
}

-(id<KASeriazableObject> ) getChildwithTypeId:(NSString *) TypeId WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude
{
    id<KASeriazableObject> ReturnValue = nil;
    
    if(![self conformsToProtocol:@protocol(KASerializeObjectLink)])
    {
        ReturnValue = [self scanChildsForItemWithID:TypeId WithDynamicallyCreatedChildsInclude:isDynamicChildsInclude];
    }
    
    if((ReturnValue == nil) && (TypeId != nil ) && ([TypeId length]>0))
    {
        NSArray<NSString *> *TypeList = [TypeId nodeIDList];
        NSInteger nCount = [TypeList count];
        if(nCount >0)
        {
            NSString *currentType = [TypeList objectAtIndex:0];
            
            ReturnValue = [self getObjectFromPathBranch:currentType WithDynamicallyCreatedChildsInclude:isDynamicChildsInclude];
            
            if( (ReturnValue != nil) && (nCount >1))
            {
                ReturnValue = [ReturnValue getChildwithTypeId:[TypeId  nextPathBranch]];
            }
        }
    }
    return ReturnValue;
}

-(id<KASeriazableObject>) getObjectFromPathBranch:(NSString *) PathBranch  WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude
{
    id<KASeriazableObject> ReturnValue =  nil;
    
    if([PathBranch isEqual:@"."])
    {
        ReturnValue = self;
    }
    else if([PathBranch isEqual:@".."])
    {
        ReturnValue = [self parent];
    }
    else if([PathBranch isObjectFamilyReference])
    {
        NSArray<id<KASeriazableObject>> *List = [self getChildListWithGroupId:[PathBranch extractObjectFamilyMarkers]];
        if([List count] == 1)
        {
            ReturnValue = [List objectAtIndex:0];
        }
    }
    else if([PathBranch isPathRoot])
    {
        ReturnValue = (id<KASeriazableObject> )[self root];
        
        NSAssert(ReturnValue != nil, @"Application object initialization not done");
    }
    else
    {
        ReturnValue = [self scanChildsForItemWithID:PathBranch WithDynamicallyCreatedChildsInclude:isDynamicChildsInclude];
    }
    return ReturnValue;
}




-(id<KASeriazableObject>) scanChildsForItemWithID:(NSString *) ID
{
    return [self scanChildsForItemWithID:ID WithDynamicallyCreatedChildsInclude:NO];
}

-(id<KASeriazableObject>) scanChildsForItemWithID:(NSString *) ID  WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude
{
    id<KASeriazableObject> ReturnValue = nil;
    
    id<KAIterator> Iterator = [self getIterator];
    
    ReturnValue = [self searchID:ID ThroughIterator:Iterator];
    if(isDynamicChildsInclude && (ReturnValue == nil))
    {
        Iterator = [self getIteratorWithDynamicChildsWithID:ID];
        ReturnValue = [self searchID:ID ThroughIterator:Iterator];
    }
    
    return ReturnValue;
}

-(id<KASeriazableObject>) searchID:(NSString * ) ID ThroughIterator:(id<KAIterator>) ITerator
{
    id<KASeriazableObject> ReturnValue = nil;
    BOOL bFound = NO;
    
    while([ITerator hasNext] && !bFound)
    {
        id<KASeriazableObjectTable> currentAttribute = (id<KASeriazableObjectTable>)[ITerator next];
        
        if(  [ID isEqual:[currentAttribute ID]])
        {
            ReturnValue = currentAttribute;
            bFound = YES;
        }
    }
    return ReturnValue;
}






-(id<KASeriazableObject> ) getChildwithIdentifier:(NSString *) Identifier
{
    id<KASeriazableObject> ReturnValue;
    
    BOOL bFound = false;
    
    id<KAIterator> Iterator = [self getIterator];
    while([Iterator hasNext])
    {
        id<KASeriazableObject> currentAttribute = [Iterator next];
        
        if(  [[currentAttribute labelIdentifier] isEqual:Identifier])
        {
            
            ReturnValue = currentAttribute;
            bFound = YES;
        }
        
    }
    return ReturnValue;
}






-(void) addDataForDynamicChildsIteration:(id<KASeriazableObject>) Data
{
    if(self.additionalDataForIteration == nil)
    {
        self.additionalDataForIteration = [[KASeriazableObjectTableImp alloc] initWithLabel:@"additonal dynamiclly created data" WithLabelIdentifier:@"aditionalData" WithObjectFamilyName:[KASeriazableObjectTableImp getTypeTag] WithID:@"aditionalData"];
    }
    [self.additionalDataForIteration addChild:Data];
}

-(id<KAIterator>) getIteratorWithDynamicChildsWithID:(NSString *) ID
{
    id<KAIterator> ReturnValue = nil;
    NSArray<id<KASeriazableObjectTable>> * List = [self getDataForDynamicChildIterationWithID:ID];
    
    for(int i = 0; i< [List count]; i++)
    {
        [self addDataForDynamicChildsIteration:[List objectAtIndex:i]];
    }
    
    ReturnValue = self.additionalDataForIteration.getIterator;
    return ReturnValue;
}


@end

