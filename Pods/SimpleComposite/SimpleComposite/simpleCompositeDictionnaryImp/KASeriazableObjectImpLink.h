//
//  KASeriazableObjectImpLink.h
//  kakebo
//
//  Created by Didier Lobeau on 23/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectTableImp.h"
#import "KASerializeObjectLink.h"


@interface KASeriazableObjectImpLink : KASeriazableObjectTableImp<KASerializeObjectLink>

@property id<KASeriazableObject> sourceObject;


@end
