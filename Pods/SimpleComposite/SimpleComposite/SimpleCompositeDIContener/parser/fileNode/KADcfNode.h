//
//  KADcfNode.h
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol KASerializeObjectFactory;
@protocol KASeriazableObject;
@protocol KADcfNode <NSObject>

-(NSDictionary *) getNodeAttributeDictionnary;

-(NSArray<id<KADcfNode>> *) getNodeChildList;

-(NSString *) getNodeName;
-(void) setNodeName:(NSString *) Name;

-(id<KASerializeObjectFactory>) factoryDelegate;
-(void) setFactoryDelegate:(id<KASerializeObjectFactory>) factoryDelegate;

-(id<KASeriazableObject>) ParseNodeWithPath:(NSString *) Path;

@end
