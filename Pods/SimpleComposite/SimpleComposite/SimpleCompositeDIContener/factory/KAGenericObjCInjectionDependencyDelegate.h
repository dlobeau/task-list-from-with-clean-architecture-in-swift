//
//  KAGenericObjCInjectionDependencyDelegate.h
//  SimpleComposite
//
//  Created by Didier Lobeau on 08/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAInjectionDependencyDelegate.h"
NS_ASSUME_NONNULL_BEGIN

@protocol KAFile;

@interface KAGenericObjCInjectionDependencyDelegate : NSObject<KAInjectionDependencyDelegate>

-(id) initWithInjectionFileList:(NSArray<id<KAFile>> *) injectionDependencyFileList;

@end

NS_ASSUME_NONNULL_END
