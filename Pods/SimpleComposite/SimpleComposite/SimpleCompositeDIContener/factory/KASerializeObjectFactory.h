//
//  KAAtributeFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 13/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KASeriazableObject;
@protocol KASerializeObjectLink;
@protocol KAFile;

@protocol KASerializeObjectFactory <NSObject>

-(id<KASeriazableObject>) createAttributeFromDictionary:(NSDictionary *) AttributeDictionary;

-(id<KASeriazableObject>) createAttributeFromLabel:(NSString *) label
                                             WithID:(NSString *) ID
                                        WithGRoupId:(NSString *) GroupId
                                 WithTypeId:(NSString * ) TypeId;

-(id<KASeriazableObject>) createAttributeFromString:(NSString *) SerializedAttribute;

-(id<KASerializeObjectLink>) createFromSource:(id<KASeriazableObject>) Source WithID:(NSString *) ID;

-(id) createWithContentsOfFile:(id<KAFile>)File;

-(id) createObjectFromFamilyType:(NSString *)FamilyType;

- (id<KAFile>)createFileWithFileName:(NSString *)FileName;
- (id<KAFile>)createDataBaseFileWithFileName:(NSString *)FileName;

-(NSDictionary<NSString *,NSString *> *) injectionFileContentWithFileName:(NSString *) FileName;

-(id) createWithContentsOfFileWithFileName:(NSString *)FileName;

- (id)createWithContentsOfDataBaseFileWithFileName:(NSString *)FileName;
@end
