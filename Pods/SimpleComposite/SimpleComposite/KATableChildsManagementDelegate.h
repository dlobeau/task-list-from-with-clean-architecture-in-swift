//
//  KATableChildsManagementDelegate.h
//  kakebo
//
//  Created by Didier Lobeau on 10/04/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KASeriazableObject;
@protocol KASeriazableObjectTable;
@protocol KAIterator;
@protocol KASeriazableObjectSymboleTable;

@protocol KATableChildsManagementDelegate <NSObject>

- (id<KAIterator>) getIterator;

- (NSUInteger) count;

-(void) addInChildContenerObject:(id<KASeriazableObject>) Object;

-(BOOL) emptyChildList;

-(BOOL) removeChild:(id<KASeriazableObject> ) ChildObject;
-(void) removeKey:(id<KASeriazableObject>) Key withRank:(NSInteger) Rank;

-(void) removeObjectAtIndex:(NSInteger) Index;
-(NSArray<id<KASeriazableObject>> *) childs;

-(BOOL) changeChildAtRank:(NSInteger) rank WithValue:(id<KASeriazableObject>) Value;

-(NSInteger) rankWithKey:(id<KASeriazableObject>)key;


-(id<KASeriazableObjectTable>) getValueWithKey:(id<KASeriazableObject> )Key WithKEyContener:(id<KASeriazableObjectTable>) KeyContener;



@end

NS_ASSUME_NONNULL_END
