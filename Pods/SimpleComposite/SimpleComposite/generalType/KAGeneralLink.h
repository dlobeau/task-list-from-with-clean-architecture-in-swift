//
//  KAGeneralLink.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KAComparable;
@protocol KACompleteItemIdentification;

@protocol KAGeneralLink <NSObject>

-(id<KAComparable,KACompleteItemIdentification>) source;
-(void) setSource:(id<KAComparable,KACompleteItemIdentification>) Source;

-(void) fetchSource;

-(NSString *) sourcePath;

@end

NS_ASSUME_NONNULL_END
