//
//  KASerializable.h
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KASerializable <NSObject>

-(NSString *) toString;

@end

NS_ASSUME_NONNULL_END
