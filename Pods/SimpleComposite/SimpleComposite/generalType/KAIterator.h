//
//  KAIteraror.h
//  kakebo
//
//  Created by Didier Lobeau on 15/11/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KASeriazableObject;

@protocol KAIterator <NSObject>

-(BOOL) hasNext;
-(id<KASeriazableObject>) next;


@end
