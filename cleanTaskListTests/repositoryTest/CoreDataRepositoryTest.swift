//
//  CoreDataRepositoryTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 16/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest

class CoreDataRepositoryTest: XCTestCase
{
    var repository:Repository?
    
    
    override func setUp()
    {
        repository = RepositoryCoreData()
    }

    override func tearDown() {
        
    }

    func testCoreDataRepository_DataCreationTest()
    {
        let ReferenceDictionary = ["label":"testLabel","group_id":"testGroupeID","identifier":"testIdenfifier","type_id":"testTypeID"]
        
        repository!.create(withItemDictionary:ReferenceDictionary )
        
        let FetchedTask = repository!.request(withItemGroupId:"testGroupeID" , withItemId: "testTypeID")
        
        XCTAssertTrue("testGroupeID" == FetchedTask!.group_id,"\(FetchedTask!.group_id)instead of testGroupeID" )
    }

    

}
