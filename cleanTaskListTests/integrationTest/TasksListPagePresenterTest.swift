//
//  TasksListPagePresenterTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 15/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework
class TasksListPagePresenterTest: XCTestCase
{
    var TestEnvironment:testEnvironmentSetting?
    var app:KAApplication?
    var tasksListWindow:TaskListWindow?
    var dataBase:TaskDataBase?
    
    override func setUp()
    {
        TestEnvironment = testEnvironmentSetting()
        app = TestEnvironment!.application
        
        dataBase = TestEnvironment?.taskDataBase
     
        tasksListWindow = app?.window() as! TaskListWindow?
    }

    override func tearDown()
    {
    }

   func testtaskListPagePresenter_when2TaskInDataBase_tableShouldContain2Cell()
    {
        TestEnvironment?.createDummyTasksInDataBase()
       
        tasksListWindow!.dataBinding()
        let Section = tasksListWindow!.table?.section(at: 0)
        
        let CellCOunt = Section!.cells().count
        
        XCTAssertTrue (CellCOunt == 2, "\(CellCOunt) instead of 2")
    }

    
    func testtaskListPagePresenter_when2TaskInDataBase_pageSHouldConformTotaskDataBase()
    {
         TestEnvironment?.createDummyTasksInDataBase()
              
        tasksListWindow!.dataBinding()
         let CheckPageConformity = tasksListWindow!.testdelegate?()
        CheckPageConformity!.expectSender(tasksListWindow!, conformTo: dataBase!)
        
    }
    
   func testtaskListPagePresenter_WhenRequestCreationTaskPAgeDisplay_PresenterSHouldBeAccessible()
    {
        tasksListWindow!.dataBinding()
        let NewTaskPage = tasksListWindow?.taskCreationPage!
        
        let NewTask = dataBase?.createNewTask()!
        
        let Delegate = NewTaskPage!.testdelegate?()
        Delegate!.expectSender(NewTaskPage!, conformTo: NewTask!)
    }
    
    

  func testtaskListPagePresenter_WhenTaskisNotCompleteAndCellIsSelected_TaskShouldBeMarkAsCompleteInDataBase()
{
        TestEnvironment?.createDummyTasksInDataBase()
       
        tasksListWindow!.dataBinding()
        
        let Section = tasksListWindow!.table?.section(at: 0)
        let Cell = Section?.cell(at: 0) as! TasksListTableCell
        
        var Task = dataBase?.task(withID:Cell.id())
        XCTAssertTrue(!(Task?.status!.value())!);
        
        Cell.taskStatusCheckBox!.addCommandOnPresenterModification(withBoolean:true, withSender:  Cell.taskStatusCheckBox!)
        Cell.taskStatusCheckBox!.executeCommandHierarchy()
        
        Cell.addCommand(onChildPresenterModification: Cell.taskStatusCheckBox!, withSender: Cell)
        Cell.executeCommandHierarchy()
         
        Task = dataBase?.task(withID:Cell.id())
        XCTAssertTrue((Task?.status!.value())!);
        
    }
 
    func testtaskListPagePresenter_WhenTaskLoadingTaskEditionPage_LoadedPageMustBeInitializedWithSelectedTask()
    {
        TestEnvironment?.createDummyTasksInDataBase()
        tasksListWindow!.dataBinding()
              
        let Section = tasksListWindow!.table?.section(at: 0)
        let Cell = Section?.cell(at: 0) as! TasksListTableCell
              
        let Task = dataBase?.task(withID:Cell.id())
        let  Window = Cell.editTaskWindow!
        let Delegate = Window.testdelegate?()
        Delegate?.expectSender(Window, conformTo: Task!)
        
    }

}
