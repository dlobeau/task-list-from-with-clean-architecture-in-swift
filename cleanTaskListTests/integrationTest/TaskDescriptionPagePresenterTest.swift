//
//  TaskDescriptionPagePresenterTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 28/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class TaskDescriptionPagePresenterTest: XCTestCase
{
    var TestEnvironment:testEnvironmentSetting?
    var app:KAApplication?
    var newTaskWindow:TaskDescriptionPage?
    var dataBase:TaskDataBase?
    var newTask:Task?
    var calendar:KACalendar?
    override func setUp()
    {
        TestEnvironment = testEnvironmentSetting()
        app = TestEnvironment!.application
           
        dataBase = TestEnvironment?.taskDataBase
        newTask = dataBase!.createNewTask()
        calendar = app?.calendar()
        newTaskWindow = TestEnvironment?.createWithContentsOfFile(WithFileName: "taskDetailPage") as? TaskDescriptionPage
           
        //data binding
        newTaskWindow?.setData(newTask)
    }

    override func tearDown()
    {
        
    }

   func testTaskCreationPagePresenterTest_TestFileInitialization()
    {
        XCTAssertNotNil(newTaskWindow);
        let Delegate = newTaskWindow?.testdelegate?()
        Delegate?.expectSender(newTaskWindow!, conformTo: newTask!)
    }
    
   func testTaskCreationPagePresenterTest_onSaveActionTriggered_NewTaskSHouldBeAdded()
    {
        let SaveEvent =  newTaskWindow?.saveButton?.event?()
        
        SaveEvent?.doAction(withSender: newTaskWindow?.saveButton!)
        
        let UnderTest = dataBase?.tasks()!.count
        
        
        XCTAssertEqual(UnderTest, 1);
    }
    
    func testTaskCreationPagePresenterTest_onModifyTaskNameAndSave_NewTaskSHouldBeAddedWithNewName()
    {
        let NewTaskName = "Finish wiki"
        
        let EditBox = newTaskWindow!.editBoxName!
        EditBox.addCommandOnLabelModification(withNewLabel: NewTaskName, withSender:EditBox)
        EditBox.executeCommandHierarchy()
        
        newTaskWindow?.addCommand(onChildPresenterModification: EditBox, withSender: newTaskWindow!)
        newTaskWindow?.executeCommandHierarchy()
        
        let SaveEvent =  newTaskWindow?.saveButton?.event?()
        SaveEvent?.doAction(withSender: newTaskWindow?.saveButton!)
        
        let NewTask = dataBase?.task(withIndex: 0)
        
        XCTAssertTrue(NewTaskName == NewTask!.name, "\(NewTask!.name!) instead of \(NewTaskName)")
    }
    
    func testTaskCreationPagePresenterTest_onModifyTaskDateFromChildDateSelectionWindow_TaskIndataBaseShouldBeUPgradedWithNewDate()
    {
        //get event on button
        let DateModificationEvent =  newTaskWindow?.deadlineSelectionButton?.event?()
        
        //get window presenter form event
        let NewDateWindow = DateModificationEvent!.getDestinationWithOwner(newTaskWindow?.deadlineSelectionButton!) as? DateSelectionPage
        
        //create new date value
        let DateReference =  calendar?.createDay(with: "2020-10-10")!
        
        //modify date window with new value, see code below
        self.ModifyDateSelectionWindow(WithWindow: NewDateWindow!, WithNewDate: DateReference!)
        
        //modify task window with new date from deadline button
        newTaskWindow?.addCommand(onChildPresenterModification: newTaskWindow?.deadlineSelectionButton!, withSender: newTaskWindow!)
        newTaskWindow!.executeCommandHierarchy()
        
        //validate modification on task window
        let SaveEvent =  newTaskWindow!.saveButton!.event?()
        SaveEvent?.doAction(withSender: newTaskWindow!.saveButton!)
        
        //extract task from database
        let NewTask = dataBase?.task(withIndex: 0)
             
        //check that modification occured on task in database
        XCTAssertTrue(DateReference!.compareAttribute(NewTask!.deadline!) == ComparisonResult.orderedSame, "\(NewTask!.deadline!.label()) instead of \(DateReference!.label())")
    }
    
    func testTaskCreationPagePresenterTest_onModifyTaskDateFromChildDateSelectionWindow_NewTaskCreatedShouldBeSetWithNewDate()
    {
        let DateModificationEvent =  newTaskWindow?.deadlineSelectionButton?.event?()
        let NewDateWindow = DateModificationEvent!.getDestinationWithOwner(newTaskWindow?.deadlineSelectionButton!) as? DateSelectionPage
              
        let DateReference =  calendar?.createDay(with: "2020-10-10")!
        self.ModifyDateSelectionWindow(WithWindow: NewDateWindow!, WithNewDate: DateReference!)
              
        let SaveEvent =  newTaskWindow!.saveButton!.event?()
        SaveEvent?.doAction(withSender: newTaskWindow!.saveButton!)
        
        let Delegate = newTaskWindow?.deadlineSelectionButton?.testdelegate?()
        Delegate?.expectSender?(newTaskWindow!.deadlineSelectionButton!,conformToText: DateReference!.completeString()!)
    }
    
    func ModifyDateSelectionWindow(WithWindow Window:DateSelectionPage,  WithNewDate NewDate:KADate)->()
       {
           let PickerDate = Window.datePicker!
           PickerDate.addCommandOnPresenterModification(withNewDate: NewDate.getObject()!, withSender: PickerDate)
           PickerDate.executeCommandHierarchy()
           
           Window.addCommandOnPresenterModificationWithPresenter(withNewValue: PickerDate, withSender: Window)
           Window.executeCommandHierarchy()
           
           let SaveEvent =  Window.saveButton?.event?()
           SaveEvent!.doAction(withSender: Window.saveButton!)
       }

}
