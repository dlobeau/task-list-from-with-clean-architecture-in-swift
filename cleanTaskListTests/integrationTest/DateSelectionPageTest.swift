//
//  DateSelectionPageTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 29/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class DateSelectionPageTest: XCTestCase
{
    var TestEnvironment:testEnvironmentSetting?
    var app:KAApplication?
    var newDateWindow:DateSelectionPage?
    var dataBase:TaskDataBase?
    var DateData:KADate?

    override func setUp()
    {
        TestEnvironment = testEnvironmentSetting()
        app = TestEnvironment!.application
           
        dataBase = TestEnvironment?.taskDataBase
        DateData = app?.calendar()?.createDay(with: "2020-10-10")
        
        newDateWindow = TestEnvironment?.createWithContentsOfFile(WithFileName: "newDateSelectionPage") as? DateSelectionPage
           
        //data binding
        newDateWindow?.setData(DateData)
    }

    override func tearDown()
    {
    }

    func testselectionPagePresenterTest_WhenIntiallizationWithDate_PickerMustBeIntiliazedWithSameDate()
    {
        let Delegate = newDateWindow?.testdelegate?()
        Delegate?.expectSender(newDateWindow!, conformTo: DateData!)
    }

    func testselectionPagePresenterTest_WhenPickerDateChangeAndPageValidation_dateWindowDataMustChageToNewSelectedDate()
    {
        let ReferenceDate = app?.calendar()?.createDay(with: "2020-12-10")
        
        let PickerDate = newDateWindow?.datePicker!
        PickerDate!.addCommandOnPresenterModification(withNewDate: ReferenceDate?.getObject()!, withSender: PickerDate)
        PickerDate!.executeCommandHierarchy();
        
        newDateWindow!.addCommandOnPresenterModificationWithPresenter(withNewValue: PickerDate!, withSender: newDateWindow!)
        newDateWindow!.executeCommandHierarchy()
        
        let SaveEvent =  newDateWindow!.saveButton?.event?()
        SaveEvent!.doAction(withSender: newDateWindow!.saveButton!)
        
        let Delegate = newDateWindow?.testdelegate?()
        Delegate?.expectSender(newDateWindow!, conformTo: ReferenceDate!)
    }

}
