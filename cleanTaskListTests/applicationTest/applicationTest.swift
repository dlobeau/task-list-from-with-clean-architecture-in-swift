//
//  applicationTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 24/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework



class applicationTest: XCTestCase
{
    var TestEnvironment:testEnvironmentSetting?
    var app:KAApplication?
    
    override func setUp()
    {
        TestEnvironment = testEnvironmentSetting()
        app = TestEnvironment!.application
    }

    override func tearDown()
    {
        
    }

    func testtestApplicationInitialization ()
    {
        let Reference:String = "Application main file"
        let UnderTest:String? = app?.label()
        
        XCTAssertTrue(Reference == UnderTest);
    }
    
   func testApplicationInitialization_DataBaseExtraction()
    {
        
         XCTAssertNotNil(app is TaskApplication);
    }


}
