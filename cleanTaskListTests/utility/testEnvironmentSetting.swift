//
//  testEnvironmentSetting.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 24/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import Swinject

class testEnvironmentSetting
{
    var TestContener:ContenerDI?
    
    private var applicationVar:KAApplication?
    var application:KAApplication?
    {
        get
        {
            if(applicationVar == nil)
            {
                TestContener = ContenerDI.init()
                
            //files used for dependency injection
                
                //creation of application instance from application delcaration file and dependency injectino list files
                KAApplicationSerializableObjectImp.createInstance(withFileName: "ApplicationMainWindowMainApp", withDIDelegate: TestContener)
                
                guard let ReturnValue =  KAApplicationSerializableObjectImp.instance()
                else
                {
                    return nil
                }
                
                
                ReturnValue.start();
                applicationVar = ReturnValue
            }
            return applicationVar
            
        }
    
    }
    
    func createWithContentsOfFile(WithFileName FileName:String )->(KAPresenter)
    {
        return application?.factory()?.createWithContentsOfFile(withFileName: FileName) as! KAPresenter
    }
    
    var taskDataBase:TaskDataBase?
    {
        get
        {
            guard let Appli = application as? TaskApplication
            else
            {
               return nil
            }
            return  Appli.taskDataBase
        }
    }
    
    var calendar:KACalendar?
    {
        get
        {
            return application?.calendar()
        }
    }
    
    func createDummyTasksInDataBase()->()
    {
        let strDate = "2020-10-10";
        let Name = "Task test 1";
        let Date = self.calendar?.createDay(with: strDate)
        let Task1 = self.taskDataBase?.createNewTask(withName: Name, WithDeadline: Date!)
        
        let strDate2 = "2020-12-10";
        let Name2 = "Task test 2";
        let Date2 = self.calendar?.createDay(with: strDate2)
               
        let Task2 = self.taskDataBase?.createNewTask(withName: Name2, WithDeadline: Date2!)
        
        Task1!.validate()
        Task2!.validate()
    }
   
}
