//
//  taskDataBaseTest.swift
//  cleanTaskListTests
//
//  Created by Didier Lobeau on 25/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class taskDataBaseTest: XCTestCase
{
    var TestEnvironment:testEnvironmentSetting?
    var app:KAApplication?
    var dataBase:TaskDataBase?
    var calendar:KACalendar?
    
   
    override func setUp()
    {
        TestEnvironment = testEnvironmentSetting();
        app = TestEnvironment!.application;
        calendar = TestEnvironment?.calendar
    }

    override func tearDown()
    {
    }

    func test_DatabaseSHouldBeintializedFromApplication_shouldBeAccessible()
    {
        dataBase = TestEnvironment?.taskDataBase;
        XCTAssertTrue(dataBase != nil);
    }
    
   func testTaskDataBase_TaskCreatedFromDataBase_sHouldNotNeNill()
    {
        dataBase = TestEnvironment?.taskDataBase;
        let strDate = "2020-10-10";
        let Name = "Task test 1";
        
        let Date = calendar?.createDay(with: strDate);
        let aTask = dataBase?.createNewTask(withName: Name, WithDeadline: Date!)
        XCTAssertTrue(Name == aTask?.name);
        XCTAssertTrue(Date?.compareAttribute(aTask?.deadline) == ComparisonResult.orderedSame);
    }
    
    func testTaskDataBase_TaskInsertedInDataBase_shouldBeretriedvedFromRequest()
    {
        dataBase = TestEnvironment?.taskDataBase;
        let strDate = "2020-10-10";
        let Name = "Task test 1";
        
        let  Date = calendar?.createDay(with: strDate);
        let aTask = dataBase?.createNewTask(withName: Name, WithDeadline: Date!)

        aTask?.validate()

        
        let ID = aTask?.id()
        let ExtractedTask = dataBase?.task(withID:ID!);
        
        let NULL_TASK:Task = (dataBase?.NULL_TASK()!)!;
        
        XCTAssertTrue( NULL_TASK.compareAttribute( ExtractedTask) != ComparisonResult.orderedSame);
        XCTAssertTrue(aTask?.compareAttribute(ExtractedTask) == ComparisonResult.orderedSame);
    }
        
    func testTaskDataBase_whenRequestTaskNotInserted_dataBAseSHouldREturnNoTask()
    {
        dataBase = TestEnvironment?.taskDataBase;
        
        let ExtractedTask = dataBase?.task(withID:"dummy ID");
        XCTAssertNotNil(dataBase?.NULL_TASK());
        XCTAssertTrue(dataBase?.NULL_TASK()?.compareAttribute(ExtractedTask) == ComparisonResult.orderedSame);
    }

   func testTaskDataBase_AllTaskInsertedInDataBase_shouldBeretriedvedFromListRequest()
    {
         dataBase = TestEnvironment?.taskDataBase;
              
        var strDate = "2020-10-10"
        var Name = "Task test 1"
        
        var Date = self.calendar?.createDay(with:strDate);
        let Task1 = dataBase?.createNewTask(withName:Name, WithDeadline:Date!)
        
        strDate = "2020-12-10";
        Name = "Task test 2";
        Date = self.calendar?.createDay(with:strDate)
        let Task2 = dataBase?.createNewTask(withName: Name, WithDeadline: Date!)
        
        Task1!.validate();
        Task2!.validate();
        
        let List = dataBase?.tasks();
        
        XCTAssertTrue(List!.count == 2, "\(List!.count) instead" );
        
        let ExtractedTask1 = self.dataBase?.task(withID: Task1!.id());
        let ExtractedTask2 = self.dataBase?.task(withID: Task2!.id());
        
        XCTAssertTrue(Task1?.compareAttribute(ExtractedTask1) == ComparisonResult.orderedSame);
        XCTAssertTrue(Task2?.compareAttribute(ExtractedTask2) == ComparisonResult.orderedSame);
    }

    func testTaskDataBase_whenTaskREmoveFromDatabase_TaskShouldNotReapperFRomRequest()
    {
        dataBase = TestEnvironment?.taskDataBase;
        
        let strDate = "2020-10-10";
        let Name = "Task test 1";
      
        let Date = self.calendar?.createDay(with:strDate);
        let Task1 = dataBase?.createNewTask(withName:Name, WithDeadline:Date!)
        
       Task1!.validate();
       Task1!.deleteItem();
        
        let List = dataBase?.tasks();
              
        XCTAssertTrue(List!.count == 0, "\(List!.count) instead" );
        
    }

   func testTaskDataBase_whenTaskREmoveFromDatabase_TaskModificationSHouldNotCreateDuplicationInDataBase()
    {
        dataBase = TestEnvironment!.taskDataBase;
        
        let strDate = "2020-10-10";
        let Name = "Task test 1";
            
        let Date = self.calendar?.createDay(with:strDate);
        let Task1:Task? = dataBase?.createNewTask(withName:Name, WithDeadline:Date!) as Task?
           
        
        Task1!.validate();
        
        Task1!.name = "new name";
        
        Task1!.validate();
          
        let List = dataBase?.tasks();
        
        XCTAssertTrue(List!.count == 1, "\(List!.count) instead 1");
        
        
    }

 
    func testTaskDataBase_whenRequestedDeadlineFromTask_shouldReturnDeadline()
    {
        dataBase = TestEnvironment!.taskDataBase;
         
         let strDate = "2020-10-10";
         let Name = "Task test 1";
             
         let ReferenceDate = self.calendar?.createDay(with:strDate);
         let Task1:Task? = dataBase?.createNewTask(withName:Name, WithDeadline:ReferenceDate!) as Task?
        
        let ExtracedDeadline:KADate? = Task1?.request(withID:"deadline") as? KADate
        
        XCTAssertTrue(ReferenceDate?.compareAttribute(ExtracedDeadline!) == ComparisonResult.orderedSame);
        
    }

    func testTaskDataBase_whenRequestedNameFromTask_shouldReturnName()
    {
       dataBase = TestEnvironment!.taskDataBase;
                
        let strDate = "2020-10-10";
        let Name = "Task test 1";
                    
        let ReferenceDate = self.calendar?.createDay(with:strDate);
        let Task1:Task? = dataBase?.createNewTask(withName:Name, WithDeadline:ReferenceDate!) as Task?
              
        let ExtracedName =  Task1?.request(withID:"name");
        
        XCTAssertTrue(Name == ExtracedName!.label(),"\(Name) instead of \(ExtracedName!.label())")
        
    }
    
   func testTaskDataBase_AllTaskRequestFromDataBase_shouldBeListOfAlltAskInDataBase()
    {
        dataBase = TestEnvironment!.taskDataBase;
                  
        let strDate = "2020-10-10";
        let Name = "Task test 1";
                      
        let Date = self.calendar?.createDay(with:strDate);
        let Task1:Task? = dataBase?.createNewTask(withName:Name, WithDeadline:Date!) as Task?
           
        let strDate2 = "2020-12-10";
        let Name2 = "Task test 2";
        let Date2  =  self.calendar?.createDay(with:strDate2);
        let Task2:Task? = dataBase?.createNewTask(withName:Name2, WithDeadline:Date2!) as Task?
           
        Task1!.validate();
        Task2!.validate();
               
        let ListObject:TaskList  = dataBase!.request(withID:"allTasks") as! TaskList;
           
        let List = ListObject.tasks();
           
        XCTAssert(List.count == 2, "\(List.count) instead of 2");
           
        let ExtractedTask1 = List[0];
         let ExtractedTask2 = List[1];
        

        XCTAssertTrue(Task1?.compareAttribute(ExtractedTask1) == ComparisonResult.orderedSame);
        XCTAssertTrue(Task2?.compareAttribute(ExtractedTask2) == ComparisonResult.orderedSame);
    }



    
}
