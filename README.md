In this repository, a simple task list coded in Swift.
![Simple tasks list app preview](./docs/taskListLowRes.mp4)

This simple task lists app is the project that illustrated the [clean architecture chosen](https://gitlab.com/dlobeau/clean-project-framework-objc). 
Our method deals with the generation of the app, based on XML description language files. 
The parser for the description files and the base classes for the project are available 
through the installation of Pod in your XCODE workspace. This architecture of the app following the clean architecture rules as proposed 
by [Robert C Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) (Uncle bob).

In this application, the [Swinject library](https://github.com/Swinject/Swinject) is used for the Dependency injection

Advantage of using the clean architecture:

1.  Application can be easily test
Indeed, with the integration test only, 80% of the application entire codebase is cover with the test (20% remaining are code for the UI).

![image1.png](./docs/image1.png)

Here is an example of how you can test an entire flow of action with integration test: we are able through the test of the presenter to execute childs window creation, click on button....
<pre><code>
func testTaskCreationPagePresenterTest_onModifyTaskDateFromChildDateSelectionWindow_TaskIndataBaseShouldBeUPgradedWithNewDate()
    {
        //get event on button
        let DateModificationEvent =  newTaskWindow?.deadlineSelectionButton?.event?()
        
        //get window presenter form event
        let NewDateWindow = DateModificationEvent!.getDestinationWithOwner(newTaskWindow?.deadlineSelectionButton!) as? DateSelectionPage
        
        //create new date value
        let DateReference =  calendar?.createDay(with: "2020-10-10")!
        
        //modify date window with new value, see code below
        self.ModifyDateSelectionWindow(WithWindow: NewDateWindow!, WithNewDate: DateReference!)
        
        //modify task window with new date from deadline button
        newTaskWindow?.addCommand(onChildPresenterModification: newTaskWindow?.deadlineSelectionButton!, withSender: newTaskWindow!)
        newTaskWindow!.executeCommandHierarchy()
        
        //validate modification on task window
        let SaveEvent =  newTaskWindow!.saveButton!.event?()
        SaveEvent?.doAction(withSender: newTaskWindow!.saveButton!)
        
        //extract task from database
        let NewTask = dataBase?.task(withIndex: 0)
             
        //check that modification occured on task in database
        XCTAssertTrue(DateReference!.compareAttribute(NewTask!.deadline!) == ComparisonResult.orderedSame, "\(NewTask!.deadline!.label()) instead of \(DateReference!.label())")
    }
    
    func ModifyDateSelectionWindow(WithWindow Window:DateSelectionPage,  WithNewDate NewDate:KADate)->()
    {
        let PickerDate = Window.datePicker!
        PickerDate.addCommandOnPresenterModification(withNewDate: NewDate.getObject()!, withSender: PickerDate)
        PickerDate.executeCommandHierarchy()
        
        Window.addCommandOnPresenterModificationWithPresenter(withNewValue: PickerDate, withSender: Window)
        Window.executeCommandHierarchy()
        
        let SaveEvent =  Window.saveButton?.event?()
        SaveEvent!.doAction(withSender: Window.saveButton!)
    }
</code></pre>




2.  Problem of the fat view controller is solved because the view is dumb
![image2.png](./docs/image2.png)
3.  Clear separation of concept which induce  easy multi threading implementation
Example coming soon
4.  Application with code entirely protocol oriented thanks to the usage of Dependency injection container 
5.  With the application entirely defined through XML files, Android and IOS application can easily evolve as they can be generated from the same unique XML file.( )Parser for android files will come soon hopefully).
