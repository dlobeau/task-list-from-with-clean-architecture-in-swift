//
//  AppDelegate.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 23/12/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

import UIKit
import cleanProjectFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
 
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        guard   let PageName = ProcessInfo.processInfo.environment["page"],
                let Mode = ProcessInfo.processInfo.environment["mode"]
        else
        {
            return false
        }
        
        if Mode != "Test"
        {
            KAApplicationSerializableObjectImp.createInstance(withFileName: PageName,withDIDelegate: ContenerDI.init())
                       
            if let app = KAApplicationSerializableObjectImp.instance()
            {
                app.start();
                        
                let MainControler:UIViewController = app.window()?.createView(withOwner: nil) as! UIViewController
                    
                self.window = UIWindow.init(frame: UIScreen.main.bounds)
                self.window?.rootViewController = MainControler
                self.window?.makeKeyAndVisible()
            }
        }
        return true
    }
   
    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }

    
   

}

