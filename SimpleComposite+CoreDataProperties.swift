//
//  SimpleComposite+CoreDataProperties.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 15/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//
//

import Foundation
import CoreData


extension SimpleComposite {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SimpleComposite> {
        return NSFetchRequest<SimpleComposite>(entityName: "SimpleComposite")
    }
    

    @NSManaged public var label: String
    @NSManaged public var group_id: String
    @NSManaged public var identifier: String
    @NSManaged public var type_id: String

}
